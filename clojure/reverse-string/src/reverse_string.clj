(ns reverse-string)

(defn move-char-to-the-end [subject]
  (str
    (subs subject 1 (count subject))
    (subs subject 0 1)))

(defn reverse-string
  ([string-to-reverse]
   (reverse-string string-to-reverse (count string-to-reverse)))

  ([string-to-reverse steps-needed]
   (if (> steps-needed 0)
     (reverse-string
       (move-char-to-the-end string-to-reverse)
       (- steps-needed 1))
     string-to-reverse)))

