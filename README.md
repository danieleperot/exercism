# Execrism

Coding exercises from [Exercism.io](https://exercism.io/).

As I don't know whether it's safe to commit the `.exercism` folder, I have added it for now on my `.gitignore`.

