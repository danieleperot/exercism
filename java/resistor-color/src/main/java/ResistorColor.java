import java.util.Arrays;
import java.util.List;

class ResistorColor {
    int colorCode(String color) {
        return colorsMap().indexOf(color);
    }

    String[] colors() {
        return new String[]{"black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"};
    }

    List<String> colorsMap() {
        return Arrays.asList(colors());
    }
}
