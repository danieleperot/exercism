import java.util.Arrays;
import java.util.List;

class ResistorColorDuo {
    int value(String[] colors) {
        int resistorValue = 0;

        int index;
        for (index = 0; index < colors.length && index < 2; index++)
            resistorValue = resistorValue * 10 + colorCode(colors[index]);

        return resistorValue;
    }

    int colorCode(String color) {
        return colorsMap().indexOf(color);
    }

    List<String> colorsMap() {
        return Arrays.asList("black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white");
    }
}
