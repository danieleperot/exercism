class ReverseString {
    String reverse(String inputString) {
        StringBuilder response = new StringBuilder();

        for (char character : inputString.toCharArray())
            response.insert(0, character);

        return response.toString();
    }
}
